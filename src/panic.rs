#[allow(unused)]
use core::{
    fmt::Write,
    panic::PanicInfo,
    sync::atomic::{compiler_fence, Ordering::SeqCst},
};
use embedded_hal::digital::v2::OutputPin;
use stm32f1xx_hal::prelude::*;

#[allow(unused)]
use rtt_target::{ChannelMode, UpChannel};

#[inline(never)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    use cortex_m::interrupt;

    interrupt::disable();

    // Light up an LED to indicate we paniced. We'll assume
    let dp = unsafe { stm32f1xx_hal::pac::Peripherals::steal() };
    let mut rcc = dp.RCC.constrain();
    let mut gpioc = dp.GPIOC.split(&mut rcc.apb2);

    gpioc
        .pc13
        .into_push_pull_output(&mut gpioc.crh)
        .set_low()
        .ok();

    if let Some(mut channel) = unsafe { UpChannel::conjure(0) } {
        channel.set_mode(ChannelMode::BlockIfFull);

        writeln!(channel, "{}", info).ok();
    }

    loop {
        compiler_fence(SeqCst);
    }
}
