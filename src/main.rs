#![no_std]
#![no_main]

use embedded_graphics::{
    image::{Image, ImageRaw},
    mono_font::MonoTextStyle,
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{PrimitiveStyle, Rectangle},
    text::Text,
};
use embedded_hal::digital::v2::InputPin;
use embedded_sdmmc::{Controller as SdController, Mode as FileMode, VolumeIdx};
use nmea0183::{ParseResult, Parser, GGA, RMC};
use ufmt::uwrite;
// use panic_rtt_target as _;
use profont::{PROFONT_14_POINT, PROFONT_24_POINT};
use rtt_target::{rprintln, rtt_init_print};

use ufmt_float::uFmt_f32;

use heapless::String;
use heapless::Vec;
use stm32f1xx_hal::{
    adc::Adc,
    delay::Delay,
    gpio::{
        gpioa::{PA5, PA6, PA7, PA8},
        gpiob::{PB0, PB1, PB12, PB13, PB14, PB15},
        Alternate, Analog, ExtiPin, Floating, Input, Output, PullUp, PushPull,
    },
    pac::{ADC1, SPI1, SPI2, TIM2, USART1},
    prelude::*,
    serial::{Config, Rx, Serial, Tx},
    spi::{Mode, Phase, Polarity, Spi, Spi1NoRemap, Spi2NoRemap},
    timer::{CountDownTimer, Timer},
};

mod display;
mod panic;

pub struct TimeSource {}

impl embedded_sdmmc::TimeSource for TimeSource {
    fn get_timestamp(&self) -> embedded_sdmmc::Timestamp {
        embedded_sdmmc::Timestamp {
            year_since_1970: 0,
            zero_indexed_month: 0,
            zero_indexed_day: 0,
            hours: 0,
            minutes: 0,
            seconds: 0,
        }
    }
}

pub type DisplaySpi = Spi<
    SPI1,
    Spi1NoRemap,
    (
        PA5<Alternate<PushPull>>,
        PA6<Input<Floating>>,
        PA7<Alternate<PushPull>>,
    ),
    u8,
>;

pub type SdSpi = Spi<
    SPI2,
    Spi2NoRemap,
    (
        PB13<Alternate<PushPull>>,
        PB14<Input<Floating>>,
        PB15<Alternate<PushPull>>,
    ),
    u8,
>;

///
pub enum SdWriteState {
    /// We can't start writing to the SD card until we know the GPS date, so we can
    /// create a file name
    Waiting,
    /// We have started writing to a file
    InProgress {
        /// The name of the file we are writing to this session. ddHHMMSS (8 bytes). Limited to 8
        /// bytes because of the requirements from embedded_sdmmc
        filename: String<8>,
        /// To save on overhead, we'll only write bytes after a certain amount has been received
        byte_buffer: Vec<u8, 256>,
    },
}

#[derive(PartialEq, Clone)]
pub enum SdActivity {
    /// The SD card was not connected
    Missing,
    /// The SD card is being written to normally
    Writing,
    /// The eject button was pressed, but we are going to write until we find a new line
    /// to avoid creating mangled NMEA strings in the logs
    Ejecting,
    /// The SD card has been fully ejected and will not be written to further
    Ejected,
    /// Some error occured with the SD card
    Error,
}

const SCREEN_SCALE: usize = 2;
const SCREEN_W: usize = 200;
const SCREEN_H: usize = 120;
const SCREEN_LINE_BITS: usize = 50;

#[rtic::app(device = stm32f1xx_hal::pac, peripherals=true)]
const APP: () = {
    struct Resources {
        eject_pin: PA8<Input<PullUp>>,
        gps_rx: Rx<USART1>,
        gps_tx: Tx<USART1>,
        display: display::SharpMemoryDisplay<
            DisplaySpi,
            PB0<Output<PushPull>>,
            SCREEN_W,
            SCREEN_H,
            SCREEN_SCALE,
            SCREEN_LINE_BITS,
        >,
        warnings_visible: bool,
        tim2: CountDownTimer<TIM2>,
        sd_controller:
            SdController<embedded_sdmmc::SdMmcSpi<SdSpi, PB12<Output<PushPull>>>, TimeSource>,
        sd_activity: SdActivity,
        nmea_parser: Parser,
        last_rmc: Option<RMC>,
        last_gga: Option<GGA>,
        sd_write_state: SdWriteState,
        adc: Adc<ADC1>,
        battery_pin: PB1<Analog>,
    }

    #[init]
    fn init(ctx: init::Context) -> init::LateResources {
        rtt_init_print!();

        rprintln!("Running init");

        let mut flash = ctx.device.FLASH.constrain();
        let mut rcc = ctx.device.RCC.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(8.mhz())
            .sysclk(72.mhz())
            .pclk1(24.mhz())
            .freeze(&mut flash.acr);
        let mut afio = ctx.device.AFIO.constrain(&mut rcc.apb2);
        let mut gpioa = ctx.device.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = ctx.device.GPIOB.split(&mut rcc.apb2);
        let mut delay = Delay::new(ctx.core.SYST, clocks);

        let mut tim2 =
            Timer::tim2(ctx.device.TIM2, &clocks, &mut rcc.apb1).start_count_down(2.hz());
        tim2.listen(stm32f1xx_hal::timer::Event::Update);

        let mut eject_pin = gpioa.pa8.into_pull_up_input(&mut gpioa.crh);
        eject_pin.make_interrupt_source(&mut afio);
        eject_pin.trigger_on_edge(&ctx.device.EXTI, stm32f1xx_hal::gpio::Edge::FALLING);
        eject_pin.enable_interrupt(&ctx.device.EXTI);

        // USART1 on Pins A9 and A10
        let pin_tx = gpiob.pb6.into_alternate_push_pull(&mut gpiob.crl);
        let pin_rx = gpiob.pb7;
        // Create an interface struct for USART1 with 9600 Baud
        let mut serial = Serial::usart1(
            ctx.device.USART1,
            (pin_tx, pin_rx),
            &mut afio.mapr,
            Config::default().baudrate(9_600.bps()),
            clocks,
            &mut rcc.apb2,
        );

        serial.listen(stm32f1xx_hal::serial::Event::Rxne);
        // separate into tx and rx channels
        let (gps_tx, gps_rx) = serial.split();

        // Display SPI
        let display_sck = gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl);
        let display_miso = gpioa.pa6;
        let display_mosi = gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl);
        let display_cs = gpiob.pb0.into_push_pull_output(&mut gpiob.crl);

        let display_spi = Spi::spi1(
            ctx.device.SPI1,
            (display_sck, display_miso, display_mosi),
            &mut afio.mapr,
            Mode {
                phase: Phase::CaptureOnFirstTransition,
                polarity: Polarity::IdleLow,
            },
            1_u32.mhz(),
            clocks,
            &mut rcc.apb2,
        );

        let mut display = display::SharpMemoryDisplay::<
            _,
            _,
            SCREEN_W,
            SCREEN_H,
            SCREEN_SCALE,
            SCREEN_LINE_BITS,
        >::new(display_spi, display_cs, &mut delay)
        .unwrap();
        display.clear().unwrap();

        // let sd_sck = gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl);
        // let sd_miso = gpioa.pa6;
        // let sd_mosi = gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl);
        // let sd_cs = gpiob.pb0.into_push_pull_output(&mut gpiob.crl);
        let sd_sck = gpiob.pb13.into_alternate_push_pull(&mut gpiob.crh);
        let sd_miso = gpiob.pb14;
        let sd_mosi = gpiob.pb15.into_alternate_push_pull(&mut gpiob.crh);
        let sd_cs = gpiob.pb12.into_push_pull_output(&mut gpiob.crh);

        let sd_spi = Spi::spi2(
            ctx.device.SPI2,
            (sd_sck, sd_miso, sd_mosi),
            Mode {
                phase: Phase::CaptureOnSecondTransition,
                polarity: Polarity::IdleHigh,
            },
            // NOTE: Init must be done at 100-400khz
            400_u32.khz(),
            clocks,
            &mut rcc.apb1,
        );

        // TODO: Use GPS as time source
        let timesource = TimeSource {};
        let mut sd_controller =
            SdController::new(embedded_sdmmc::SdMmcSpi::new(sd_spi, sd_cs), timesource);

        let sd_card_detect_pin = gpiob.pb9.into_pull_up_input(&mut gpiob.crh);
        let sd_activity = if sd_card_detect_pin.is_low().unwrap() {
            SdActivity::Missing
        } else {
            if let Err(e) = sd_controller.device().init() {
                rprintln!("SD Init error {:?}", e);
                SdActivity::Error
            } else {
                SdActivity::Writing
            }
        };

        let battery_pin = gpiob.pb1.into_analog(&mut gpiob.crl);
        let adc = Adc::adc1(ctx.device.ADC1, &mut rcc.apb2, clocks);

        init::LateResources {
            eject_pin,
            sd_activity,
            display,
            warnings_visible: false,
            tim2,
            gps_rx,
            gps_tx,
            sd_controller,
            sd_write_state: SdWriteState::Waiting,
            nmea_parser: Parser::new(),
            last_gga: None,
            last_rmc: None,
            adc,
            battery_pin,
        }
    }

    #[idle(resources = [
        display,
        last_rmc,
        last_gga,
        gps_tx,
        warnings_visible,
        sd_activity,
        adc,
        battery_pin,
    ])]
    fn idle(mut ctx: idle::Context) -> ! {
        use core::fmt::Write;
        // Set refresh interval
        ctx.resources
            .gps_tx
            .write_str("$PMTK220,500*2B\r\n")
            .unwrap();
        ctx.resources
            .gps_tx
            .write_str("$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n")
            .unwrap();

        let bottom_row_edge = 105;
        let bottom_row_text = bottom_row_edge + 10;

        let speed_location = Point::new(5, 40);
        let speed_knots_location = Point::new(105, 40);
        let speed_style = MonoTextStyle::new(&PROFONT_24_POINT, BinaryColor::On);
        let time_location = Point::new(5, 75);
        let time_style = MonoTextStyle::new(&PROFONT_24_POINT, BinaryColor::On);

        let sats_location = Point::new(184, bottom_row_text);
        let sats_style = MonoTextStyle::new(&PROFONT_14_POINT, BinaryColor::On);
        let voltage_location = Point::new(139, bottom_row_text);
        let voltage_style = MonoTextStyle::new(&PROFONT_14_POINT, BinaryColor::On);

        let units_style = MonoTextStyle::new(&PROFONT_14_POINT, BinaryColor::On);

        let r = Rectangle::new(
            Point::new(0, 0),
            Size::new(SCREEN_W as u32, SCREEN_H as u32),
        )
        .into_styled(PrimitiveStyle::with_fill(BinaryColor::Off));

        macro_rules! load_image {
            ($name:ident, $path:expr, $location:expr, $width:expr) => {
                let raw = ImageRaw::<BinaryColor>::new(include_bytes!($path), $width);
                let $name = Image::new(&raw, $location);
            };
        }

        load_image!(
            sat_image,
            "../graphics/gps.pixels",
            Point::new(184 - 12, bottom_row_edge),
            11
        );
        load_image!(
            card_eject_image,
            "../graphics/sd_card_eject.pixels",
            Point::new(114, bottom_row_edge),
            11
        );
        load_image!(
            card_missing_image,
            "../graphics/sd_card_missing.pixels",
            Point::new(114, bottom_row_edge),
            11
        );
        load_image!(
            card_error_image,
            "../graphics/sd_card_error.pixels",
            Point::new(114, bottom_row_edge),
            11
        );
        load_image!(
            battery_image,
            "../graphics/battery.pixels",
            Point::new(133, bottom_row_edge),
            5
        );

        let mut chars = String::<32>::new();

        loop {
            r.draw(ctx.resources.display).unwrap();
            draw_text!(
                chars, ctx.resources.display, Point::new(5, speed_location.y - 28), units_style
                => "m/s:"
            );
            draw_text!(
                chars, ctx.resources.display, Point::new(105, speed_knots_location.y - 28), units_style
                => "knots:"
            );

            match ctx.resources.last_rmc.lock(|rmc| rmc.clone()) {
                Some(rmc) => {
                    let time = rmc.datetime.time;
                    draw_text!(
                        chars, ctx.resources.display, time_location, time_style
                        // +2 since GPS time is UTC
                        => "{}:{}:{}", TimeNum(time.hours+2), TimeNum(time.minutes), TimeNum(time.seconds as u8)
                    );
                    let speed_knots = rmc.speed.as_mps() * 1.943844;
                    draw_text!(
                        chars, ctx.resources.display, speed_location, speed_style
                        => "{}", uFmt_f32::Two(rmc.speed.as_mps())
                    );
                    draw_text!(
                        chars, ctx.resources.display, speed_knots_location, speed_style
                        => "{:.2}", speed_knots
                    );
                }
                None => {
                    draw_text!(
                        chars, ctx.resources.display, time_location, time_style
                        => "--:--:--"
                    );
                    draw_text!(
                        chars, ctx.resources.display, speed_location, speed_style
                        => "--.--"
                    );
                    draw_text!(
                        chars, ctx.resources.display, speed_knots_location, speed_style
                        => "--.--"
                    );
                }
            };

            match ctx.resources.last_gga.lock(|gga| gga.clone()) {
                Some(gga) => {
                    draw_text!(
                        chars, ctx.resources.display, sats_location, sats_style
                        => "{}", gga.sat_in_use
                    );
                }
                None => {
                    draw_text!(
                        chars, ctx.resources.display, sats_location, sats_style
                        => "?"
                    );
                }
            }

            let voltage_raw: u16 = ctx.resources.adc.read(ctx.resources.battery_pin).unwrap();
            // 10 bit ADC, with a voltage divider producing 3.173 V at a battery voltage of 4.2 V
            let voltage = voltage_raw as f32 / (1 << 12) as f32 * 3.3 / 3.173 * 4.2;

            draw_text!(chars, ctx.resources.display, voltage_location, voltage_style
                => "{}", uFmt_f32::One(voltage)
            );

            sat_image.draw(ctx.resources.display).unwrap();
            battery_image.draw(ctx.resources.display).unwrap();

            let sd_state = ctx.resources.sd_activity.lock(|a| a.clone());
            let (sd_image, blink_sd) = match sd_state {
                SdActivity::Missing => (Some(card_missing_image), true),
                SdActivity::Ejected => (Some(card_eject_image), true),
                SdActivity::Ejecting => (Some(card_eject_image), false),
                SdActivity::Writing => (None, false),
                SdActivity::Error => (Some(card_error_image), true),
            };

            if let Some(sd_image) = sd_image {
                let show_sd_image = !blink_sd || ctx.resources.warnings_visible.lock(|w| *w);
                if show_sd_image {
                    sd_image.draw(ctx.resources.display).unwrap();
                }
            }
            ctx.resources.display.draw_buffer().unwrap();
        }
    }

    #[task(binds = USART1, resources = [gps_rx, nmea_parser, last_rmc, last_gga], spawn = [byte_writer], priority=2)]
    fn usart0(ctx: usart0::Context) {
        let byte = match ctx.resources.gps_rx.read() {
            Ok(b) => b,
            Err(e) => {
                rprintln!("GPS read error: {:?}", e);
                return;
            }
        };

        // Ignoring this is kind of dangerous because we'll end up not writing some bytes
        // to the sd card. However, a good nmea parser should be able to recover from such an
        // error,
        // and that way we don't panic the rest of the system
        let _ = ctx.spawn.byte_writer(byte);

        for result in ctx.resources.nmea_parser.parse_from_byte(byte) {
            match result {
                Ok(ParseResult::RMC(Some(rmc))) => {
                    *ctx.resources.last_rmc = Some(rmc);
                }
                Ok(ParseResult::GGA(Some(gga))) => {
                    *ctx.resources.last_gga = Some(gga);
                }
                Ok(_) => {}
                Err(e) => {
                    rprintln!("GPS read error {:?}", e)
                }
            }
        }
    }

    // We need fairly high capacity for this task since SD writing seems to take some time,
    // and we need enough buffer space to cache all the GPS uart bytes until we're ready to receive
    // more
    #[task(
        resources = [sd_controller, sd_write_state, last_rmc, sd_activity],
        priority=1,
        capacity=128
    )]
    fn byte_writer(mut ctx: byte_writer::Context, byte: u8) {
        // If the card is ejected, we should stop writing to the SD card
        let sd_activity = ctx.resources.sd_activity.clone();
        match sd_activity {
            SdActivity::Missing | SdActivity::Ejected | SdActivity::Error => {
                return;
            }
            SdActivity::Writing | SdActivity::Ejecting => {}
        }
        if sd_activity == SdActivity::Missing || sd_activity == SdActivity::Ejected {
            return;
        }

        if sd_activity == SdActivity::Ejecting {
            if byte == b'\n' {
                *ctx.resources.sd_activity = SdActivity::Ejected;
                return;
            }
        }
        let new_state = match ctx.resources.sd_write_state {
            SdWriteState::Waiting => {
                // Check if we have a new rmc message
                if let Some(rmc) = ctx.resources.last_rmc.lock(|rmc| rmc.clone()) {
                    let mut filename = String::new();
                    uwrite!(
                        filename,
                        "{}{}{}{}",
                        TimeNum(rmc.datetime.date.day),
                        TimeNum(rmc.datetime.time.hours),
                        TimeNum(rmc.datetime.time.minutes),
                        TimeNum(rmc.datetime.time.seconds as u8),
                    )
                    .unwrap();
                    rprintln!("Starting a new log file {}", filename);
                    Some(SdWriteState::InProgress {
                        byte_buffer: Vec::new(),
                        filename,
                    })
                } else {
                    Some(SdWriteState::Waiting)
                }
            }
            SdWriteState::InProgress {
                byte_buffer,
                filename,
            } => {
                byte_buffer.push(byte).unwrap();

                if byte_buffer.len() == byte_buffer.capacity() {
                    let controller = ctx.resources.sd_controller;

                    macro_rules! try_sd_op {
                        ($operation:expr, $description:expr) => {
                            match $operation {
                                Ok(result) => result,
                                Err(e) => {
                                    rprintln!("{}\n\t{:?}", $description, e);
                                    *ctx.resources.sd_activity = SdActivity::Error;
                                    return;
                                }
                            }
                        };
                    }

                    let mut volume =
                        try_sd_op!(controller.get_volume(VolumeIdx(0)), "failed to get volume");

                    let root_dir =
                        try_sd_op!(controller.open_root_dir(&volume), "Failed to open root dir");

                    let mut file = try_sd_op!(
                        controller.open_file_in_dir(
                            &mut volume,
                            &root_dir,
                            filename,
                            FileMode::ReadWriteCreateOrAppend,
                        ),
                        "Failed to open file"
                    );

                    let mut written = 0;
                    while written < byte_buffer.len() {
                        written += try_sd_op!(
                            controller.write(&mut volume, &mut file, &byte_buffer[written..]),
                            "Failed to write bytes"
                        );
                    }
                    byte_buffer.clear();

                    controller.close_file(&volume, file).unwrap();
                    controller.close_dir(&volume, root_dir);
                }
                None
            }
        };

        if let Some(new_state) = new_state {
            *ctx.resources.sd_write_state = new_state;
        }
    }

    #[task(binds = EXTI9_5, resources=[eject_pin, sd_activity])]
    fn exti9_5(ctx: exti9_5::Context) {
        ctx.resources.eject_pin.clear_interrupt_pending_bit();

        if let SdActivity::Writing = *ctx.resources.sd_activity {
            *ctx.resources.sd_activity = SdActivity::Ejecting;
        }

        rprintln!("Card eject button read")
    }

    #[task(binds = TIM2, resources = [tim2, warnings_visible])]
    fn tim2(ctx: tim2::Context) {
        ctx.resources.tim2.clear_update_interrupt_flag();
        *ctx.resources.warnings_visible = !*ctx.resources.warnings_visible;
    }

    extern "C" {
        fn CAN_RX1();
    }
};

struct TimeNum(u8);
impl ufmt::uDisplay for TimeNum {
    fn fmt<W>(&self, f: &mut ufmt::Formatter<'_, W>) -> Result<(), W::Error>
    where
        W: ufmt::uWrite + ?Sized,
    {
        let val = self.0;
        if val < 10 {
            uwrite!(f, "0{}", val)
        } else {
            uwrite!(f, "{}", val)
        }
    }
}
