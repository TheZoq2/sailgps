EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L bp:BP U1
U 1 1 60F812AF
P 5600 4000
F 0 "U1" H 5650 4200 60  0000 C CNN
F 1 "BP" H 5650 4100 60  0000 C CNN
F 2 "blue-pill-kicad:blue_pill" H 5500 4750 60  0001 C CNN
F 3 "https://www.electronicshub.org/getting-started-with-stm32f103c8t6-blue-pill/" H 5500 4750 60  0001 C CNN
	1    5600 4000
	1    0    0    -1  
$EndComp
$Comp
L local_breakouts:micro_sd_breakout U4
U 1 1 60F86136
P 8300 3300
F 0 "U4" H 8200 3250 50  0000 L CNN
F 1 "micro_sd_breakout" V 8100 2900 50  0000 L CNN
F 2 "sailgps:micro_sd_breakout" H 8300 3300 50  0001 C CNN
F 3 "" H 8300 3300 50  0001 C CNN
	1    8300 3300
	1    0    0    -1  
$EndComp
$Comp
L local_breakouts:adafruit_ultimate_gps U2
U 1 1 60F870DB
P 8250 4600
F 0 "U2" V 8346 4522 50  0000 R CNN
F 1 "adafruit_ultimate_gps" V 8255 4522 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 8250 4600 50  0001 C CNN
F 3 "" H 8250 4600 50  0001 C CNN
	1    8250 4600
	0    -1   -1   0   
$EndComp
$Comp
L Device:Battery_Cell BT1
U 1 1 60F8A793
P 7900 5800
F 0 "BT1" V 7650 5800 50  0000 C CNN
F 1 "Battery_Cell" V 7750 5750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 7900 5860 50  0001 C CNN
F 3 "~" V 7900 5860 50  0001 C CNN
	1    7900 5800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 60F90F67
P 5750 5050
F 0 "#PWR02" H 5750 4800 50  0001 C CNN
F 1 "GND" H 5755 4877 50  0000 C CNN
F 2 "" H 5750 5050 50  0001 C CNN
F 3 "" H 5750 5050 50  0001 C CNN
	1    5750 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 60F912CC
P 7800 5800
F 0 "#PWR09" H 7800 5550 50  0001 C CNN
F 1 "GND" V 7805 5672 50  0000 R CNN
F 2 "" H 7800 5800 50  0001 C CNN
F 3 "" H 7800 5800 50  0001 C CNN
	1    7800 5800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 60F91739
P 7750 3450
F 0 "#PWR05" H 7750 3200 50  0001 C CNN
F 1 "GND" V 7755 3322 50  0000 R CNN
F 2 "" H 7750 3450 50  0001 C CNN
F 3 "" H 7750 3450 50  0001 C CNN
	1    7750 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 60F93D81
P 6400 5700
F 0 "R1" V 6193 5700 50  0000 C CNN
F 1 "R" V 6284 5700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6330 5700 50  0001 C CNN
F 3 "~" H 6400 5700 50  0001 C CNN
	1    6400 5700
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60F94ECC
P 6850 5700
F 0 "R2" V 6643 5700 50  0000 C CNN
F 1 "R" V 6734 5700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6780 5700 50  0001 C CNN
F 3 "~" H 6850 5700 50  0001 C CNN
	1    6850 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	6550 5700 6600 5700
$Comp
L power:GND #PWR03
U 1 1 60F955B2
P 7000 5700
F 0 "#PWR03" H 7000 5450 50  0001 C CNN
F 1 "GND" V 7005 5572 50  0000 R CNN
F 2 "" H 7000 5700 50  0001 C CNN
F 3 "" H 7000 5700 50  0001 C CNN
	1    7000 5700
	0    -1   -1   0   
$EndComp
Text GLabel 8100 5800 2    50   Input ~ 0
vbat
Text GLabel 6250 5700 0    50   Input ~ 0
vbat
Text GLabel 7800 4250 0    50   Input ~ 0
vbat
Text GLabel 6600 5900 3    50   Input ~ 0
vsens
Wire Wire Line
	6600 5900 6600 5700
Connection ~ 6600 5700
Wire Wire Line
	6600 5700 6700 5700
Text GLabel 4850 4400 0    50   Input ~ 0
vsens
Text Notes 6150 6300 0    50   ~ 0
Battery monitoring\n
Wire Notes Line
	7400 5350 7400 6350
Wire Notes Line
	5750 6350 5750 5350
$Comp
L Switch:SW_Push SW1
U 1 1 60FA40AB
P 4950 5900
F 0 "SW1" H 4950 6185 50  0000 C CNN
F 1 "SW_Push" H 4950 6094 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4950 6100 50  0001 C CNN
F 3 "~" H 4950 6100 50  0001 C CNN
	1    4950 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60FA540C
P 4750 5900
F 0 "#PWR01" H 4750 5650 50  0001 C CNN
F 1 "GND" V 4755 5772 50  0000 R CNN
F 2 "" H 4750 5900 50  0001 C CNN
F 3 "" H 4750 5900 50  0001 C CNN
	1    4750 5900
	0    1    1    0   
$EndComp
Text GLabel 5150 5900 2    50   Input ~ 0
eject_pin
Wire Notes Line
	4400 5350 4400 6350
Text GLabel 6350 3300 2    50   Input ~ 0
eject_pin
Text GLabel 7800 4450 0    50   Input ~ 0
gps_rx
Text GLabel 7800 4550 0    50   Input ~ 0
gps_tx
Text GLabel 6350 3400 2    50   Input ~ 0
gps_tx
Text GLabel 6350 3500 2    50   Input ~ 0
gps_rx
$Comp
L local_breakouts:memory_display U3
U 1 1 60FBAE15
P 8300 2150
F 0 "U3" V 8346 2072 50  0000 R CNN
F 1 "memory_display" V 8255 2072 50  0000 R CNN
F 2 "sailgps:sharp_display" H 8300 2150 50  0001 C CNN
F 3 "" H 8300 2150 50  0001 C CNN
	1    8300 2150
	0    -1   -1   0   
$EndComp
Text GLabel 7800 2050 0    50   Input ~ 0
DispCLK
Text GLabel 7800 2150 0    50   Input ~ 0
DispMOSI
Text GLabel 7800 2250 0    50   Input ~ 0
DipsCS
$Comp
L power:GND #PWR07
U 1 1 60FC1473
P 7800 1950
F 0 "#PWR07" H 7800 1700 50  0001 C CNN
F 1 "GND" V 7805 1822 50  0000 R CNN
F 2 "" H 7800 1950 50  0001 C CNN
F 3 "" H 7800 1950 50  0001 C CNN
	1    7800 1950
	0    1    1    0   
$EndComp
Text GLabel 4850 4000 0    50   Input ~ 0
DispCLK
Text GLabel 4850 4200 0    50   Input ~ 0
DispMOSI
Text GLabel 4850 4300 0    50   Input ~ 0
DipsCS
Text GLabel 7750 3150 0    50   Input ~ 0
SD_MOSI
Text GLabel 7750 3350 0    50   Input ~ 0
SD_SCK
Text GLabel 7750 3550 0    50   Input ~ 0
SD_MISO
Text GLabel 7750 3650 0    50   Input ~ 0
SD_CD
Text GLabel 7750 3050 0    50   Input ~ 0
SD_CS
Text GLabel 6350 4700 2    50   Input ~ 0
SD_SCK
Text GLabel 6350 4500 2    50   Input ~ 0
SD_MOSI
Text GLabel 6350 4600 2    50   Input ~ 0
SD_MISO
Text GLabel 6350 4800 2    50   Input ~ 0
SD_CS
Text GLabel 6350 3200 2    50   Input ~ 0
SD_CD
$Comp
L power:+3V3 #PWR06
U 1 1 60FD3185
P 7800 1750
F 0 "#PWR06" H 7800 1600 50  0001 C CNN
F 1 "+3V3" V 7815 1878 50  0000 L CNN
F 2 "" H 7800 1750 50  0001 C CNN
F 3 "" H 7800 1750 50  0001 C CNN
	1    7800 1750
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR04
U 1 1 60FD3E85
P 7750 3250
F 0 "#PWR04" H 7750 3100 50  0001 C CNN
F 1 "+3V3" V 7765 3378 50  0000 L CNN
F 2 "" H 7750 3250 50  0001 C CNN
F 3 "" H 7750 3250 50  0001 C CNN
	1    7750 3250
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR08
U 1 1 60FD5AE7
P 7800 4950
F 0 "#PWR08" H 7800 4800 50  0001 C CNN
F 1 "+3V3" V 7815 5078 50  0000 L CNN
F 2 "" H 7800 4950 50  0001 C CNN
F 3 "" H 7800 4950 50  0001 C CNN
	1    7800 4950
	0    -1   -1   0   
$EndComp
Wire Notes Line
	8500 5350 8500 6350
Wire Notes Line
	4400 5350 8500 5350
Wire Notes Line
	4400 6350 8500 6350
$Comp
L power:+3V3 #PWR0101
U 1 1 60F887E5
P 5600 2950
F 0 "#PWR0101" H 5600 2800 50  0001 C CNN
F 1 "+3V3" V 5615 3078 50  0000 L CNN
F 2 "" H 5600 2950 50  0001 C CNN
F 3 "" H 5600 2950 50  0001 C CNN
	1    5600 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 60F98500
P 7800 4350
F 0 "#PWR0102" H 7800 4100 50  0001 C CNN
F 1 "GND" V 7805 4222 50  0000 R CNN
F 2 "" H 7800 4350 50  0001 C CNN
F 3 "" H 7800 4350 50  0001 C CNN
	1    7800 4350
	0    1    1    0   
$EndComp
$EndSCHEMATC
