use std::path::PathBuf;

use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    #[structopt()]
    input: PathBuf,
    #[structopt()]
    width: usize,
    #[structopt(short="o")]
    output: PathBuf,
}

fn to_bits(width: usize, pixels: &[u8]) -> Vec<u8> {
    let mut pixels_seen_this_line = 0;
    let mut bit = 0;
    let mut byte = 0;
    let mut result = vec![0];

    for pixel in pixels {
        if *pixel != 0 {
            result[byte] |= 1 << (7 - bit);
        }


        pixels_seen_this_line += 1;
        if pixels_seen_this_line < width {
            if bit == 7 {
                bit = 0;
                byte += 1;
                result.push(0)
            }
            else {
                bit += 1;
            }
        }
        else {
            bit = 0;
            byte += 1;
            result.push(0);
            pixels_seen_this_line = 0;
        }

    }

    result
}

fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();

    let bytes = std::fs::read(opt.input)?;

    std::fs::write(opt.output, to_bits(opt.width, &bytes))?;

    Ok(())
}
